import {
  cvToHex,
  cvToValue,
  parseReadOnlyResponse,
  stringAsciiCV,
} from "@stacks/transactions";
import {
  getContractName,
  getContractOwner,
  getStacksAPIPrefix,
} from "../../services/auth";
import { convertToPersonScheme } from "../../services/utils";

export default async function handler(req, res) {
  const { dns } = req.query;

  const { profileFound, owlLinkProfile } = await getOwlLinkProfile(dns);

  if (profileFound && owlLinkProfile) {
    res.status(200).json(owlLinkProfile);
  } else {
    res
      .status(500)
      .json({ status: "error", message: "Owl link profile is not found." });
  }
}

async function getOwlLinkProfile(dns) {
  let owlLinkProfile = {};
  let profileFound = false;

  try {
    // If dns is available, then proceed
    if (dns) {
      // Fetch gaia URL from stacks blockchain
      const rawResponse = await fetch(
        getStacksAPIPrefix() +
          "/v2/contracts/call-read/" +
          getContractOwner() +
          "/" +
          getContractName() +
          "/get-token-uri",
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            sender: getContractOwner(),
            arguments: [cvToHex(stringAsciiCV(dns))],
          }),
        }
      );
      const content = await rawResponse.json();

      // If data found on stacks blockchain
      if (content && content.okay) {
        // Get gaia address
        const gaiaProfileUrl = cvToValue(parseReadOnlyResponse(content)).value
          .value;

        try {
          // Fetch data from external API
          const res = await fetch(gaiaProfileUrl);
          owlLinkProfile = await res.json();

          // Convert to Person Schema
          convertToPersonScheme(owlLinkProfile);

          // Update default NFT images

          // Turn the flag on
          profileFound = true;
        } catch (error) {
          // Error reading file from gaia, but btc domain already minted.

          // Turn the flag on
          profileFound = true;
        }
      }
    }
  } catch (error) {
    // Turn the flag off
    profileFound = false;
  }

  return {
    owlLinkProfile,
    profileFound,
  };
}
